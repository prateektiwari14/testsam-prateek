﻿using FizzBuzz.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace FizzBuzz.Web.Controllers
{
    public class FizzBuzzController : ApiController
    {
        private readonly IFizzBuzzRepository _fizzBuzzRepository;

        public FizzBuzzController(IFizzBuzzRepository fizzBuzzRepository)
        {
            _fizzBuzzRepository = fizzBuzzRepository;
        }


        public IEnumerable<string> Get(int id)
        {
            return _fizzBuzzRepository.PlayFizzBuzz(id);
        }
    }
}