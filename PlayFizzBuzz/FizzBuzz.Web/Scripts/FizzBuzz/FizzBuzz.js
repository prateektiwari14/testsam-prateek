﻿function fizzBuzzCtrl($scope, $http) {
    $scope.input = 1;
    $scope.result = "";

    $scope.updateFizzBuzz = function (number) {
        if (number != "" && number != undefined) {
            if (parseInt(number) > 0 && parseInt(number) <= 100) {
                $http.get('/api/fizzbuzz/' + number).success(function (data) {
                    $scope.result = data;
                })
            }
            else {
                alert("Number should be in range of 1 to 100.");
            }
        }
    }
}