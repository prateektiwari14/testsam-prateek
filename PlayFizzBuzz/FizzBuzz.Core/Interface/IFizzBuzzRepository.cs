﻿using System;
using System.Collections.Generic;

namespace FizzBuzz.Core
{
    public interface IFizzBuzzRepository
    {
        IEnumerable<string> PlayFizzBuzz(int value);

        DayOfWeek DayOfWeek { get; }
    }
}
