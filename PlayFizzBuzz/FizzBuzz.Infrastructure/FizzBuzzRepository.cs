﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzz.Core;
using System.Globalization;


namespace FizzBuzz.Infrastructure
{
    public class FizzBuzzRepository : IFizzBuzzRepository
    {
        public DayOfWeek DayOfWeek
        {
            get { return DateTime.Now.DayOfWeek; }
        }

        public IEnumerable<string> PlayFizzBuzz(int value)
        {
            if (value > 0)
            {
                return Enumerable.Range(1, value).Select(GenerateFizzBuzz);
            }
            else
            {
                return null;
            }
        }

        public string GenerateFizzBuzz(int value)
        {
            if (value % 15 == 0)
                return DayOfWeek != DayOfWeek.Wednesday ? "Fizz Buzz" : "Wizz Wuzz";
            if (value % 5 == 0)
                return DayOfWeek != DayOfWeek.Wednesday ? "Buzz" : "Wuzz";
            if (value % 3 == 0)
                return DayOfWeek != DayOfWeek.Wednesday ? "Fizz" : "Wizz";
            else
                return value.ToString(CultureInfo.CurrentCulture);
        }
    }
}