﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzz.Infrastructure;
using NUnit.Framework;
using NUnit.Framework.Constraints;


namespace FizzBuzz.NUnit.Tests
{
    [TestFixture()]
    public class FizzBuzzRepositoryTests
    {
        private readonly FizzBuzzRepository fizzBuzzRepository;

        public FizzBuzzRepositoryTests()
        {
            fizzBuzzRepository = new FizzBuzzRepository();
        }


        [Test]
        public void NegativeValuesThrowsArgumentException()
        {
            fizzBuzzRepository.PlayFizzBuzz(-1);
        }


        [TestCase(3, ExpectedResult = "Fizz")]
        [TestCase(6, ExpectedResult = "Fizz")]
        [TestCase(9, ExpectedResult = "Fizz")]
        public string NumberDividedByThreeReturnedAsFizz(int value)
        {
            return fizzBuzzRepository.GenerateFizzBuzz(value);
        }


        [TestCase(5, ExpectedResult = "Buzz")]
        [TestCase(10, ExpectedResult = "Buzz")]
        [TestCase(20, ExpectedResult = "Buzz")]
        public string NumberDividedByFiveReturnedAsBuzz(int value)
        {
            return fizzBuzzRepository.GenerateFizzBuzz(value);
        }

        [TestCase(0, ExpectedResult = "Fizz Buzz")]
        [TestCase(1, ExpectedResult = "1")]
        [TestCase(2, ExpectedResult = "2")]
        [TestCase(3, ExpectedResult = "Fizz")]
        [TestCase(4, ExpectedResult = "4")]
        [TestCase(5, ExpectedResult = "Buzz")]
        [TestCase(6, ExpectedResult = "Fizz")]
        [TestCase(10, ExpectedResult = "Buzz")]
        [TestCase(15, ExpectedResult = "Fizz Buzz")]
        [TestCase(45, ExpectedResult = "Fizz Buzz")]
        public string FizzBuzzExpectedResults(int value)
        {
            return fizzBuzzRepository.GenerateFizzBuzz(value);
        }

    }
}
